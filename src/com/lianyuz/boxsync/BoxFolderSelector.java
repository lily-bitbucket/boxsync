package com.lianyuz.boxsync;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Stack;

import com.box.androidlib.Box;
import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;
import com.box.androidlib.ResponseListeners.CreateFolderListener;
import com.box.androidlib.ResponseListeners.GetAccountTreeListener;

import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.text.format.DateFormat;

public class BoxFolderSelector extends ListActivity  {

    private MyArrayAdapter adapter;
    private TreeListItem[] items;
    private String authToken;
    
    private PathStack pathStack;

    Button selectButton;
	Button cancelButton;
	Button upButton;
	Button createFolderButton;
	TextView pathTextView;
	ProgressBar progressBar;
	View myView;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder_selector);

        // Check if we have an Auth Token stored.
        final SharedPreferences prefs = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
        authToken = prefs.getString(Constants.PREFS_KEY_AUTH_TOKEN, null);
        if (authToken == null) {
            Toast.makeText(getApplicationContext(), getString(R.string.toast_cannot_log_in), Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        // View your root folder by default (folder_id = 0l), or this activity
        // can also be launched to view subfolders
        pathStack = new PathStack();
        pathStack.pushFolderAndId("", 0l);
        
        pathTextView = (TextView) findViewById(R.id.folder_selector_path);
        pathTextView.setText(pathStack.getVerbosePath());
        selectButton = (Button) findViewById(R.id.button_folder_selector_select);
        selectButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	Intent intent = new Intent();
            	intent.putExtra("box_sync_folder", pathStack.getPath());
            	intent.putExtra("box_sync_folder_id", pathStack.getTopFolderId());
                setResult(MainSyncStatus.BOX_SYNC_FOLDER_SELECT, intent);
                finish();
            }
        });
        cancelButton = (Button) findViewById(R.id.button_folder_selector_cancel);
        cancelButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        upButton = (Button) findViewById(R.id.button_folder_selector_up);
        upButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            	if (pathStack.size() > 1) {
            		pathStack.pop();
            		pathTextView.setText(pathStack.getVerbosePath());
            		refresh();
            	}
            }
        });
        
        createFolderButton = (Button) findViewById(R.id.button_folder_selector_createFolder);
		createFolderButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				LayoutInflater factory = LayoutInflater
						.from(BoxFolderSelector.this);
				myView = factory
						.inflate(R.layout.pop_up_give_folder_name, null);
				final EditText fileNameEditText = (EditText) myView
						.findViewById(R.id.et_newfolderName);
				AlertDialog.Builder builder = new AlertDialog.Builder(
						BoxFolderSelector.this).setPositiveButton(getString(R.string.OK),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								String newFolderName = fileNameEditText
										.getText().toString();
								if (newFolderName != null && !newFolderName.equals("")) {
									final Box box = Box.getInstance(Constants.API_KEY);
									box.createFolder(authToken, pathStack.getTopFolderId(), newFolderName, false, new CreateFolderListener() {
	
					                    @Override
					                    public void onComplete(final BoxFolder boxFolder, final String status) {
					                        if (status.equals(CreateFolderListener.STATUS_CREATE_OK)) {
					                            Toast.makeText(getApplicationContext(), getString(R.string.toast_folder_created), Toast.LENGTH_SHORT).show();
					                            refresh();
					                        }
					                        else {
					                            Toast.makeText(getApplicationContext(), getString(R.string.toast_folder_creation_failed), Toast.LENGTH_LONG).show();
					                        }
					                    }
	
					                    @Override
					                    public void onIOException(final IOException e) {
					                        e.printStackTrace();
					                        Toast.makeText(getApplicationContext(), getString(R.string.toast_folder_creation_failed), Toast.LENGTH_LONG).show();
					                    }
					                });
								}
							}
						}).setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
				AlertDialog nameDialog = builder.create();
				nameDialog.setView(myView);
				nameDialog.show();
			}
		});
        
        progressBar = (ProgressBar) findViewById(R.id.folder_selector_progress);
        
        
        
        
        // Initialize list items and set adapter
        items = new TreeListItem[0];
        adapter = new MyArrayAdapter(this, 0, items);
        setListAdapter(adapter);
        // Go get the account tree
        refresh();
    }

    /**
     * Refresh the tree.
     */
    private void refresh() {
    	progressBar.setVisibility(View.VISIBLE);
    	
        final Box box = Box.getInstance(Constants.API_KEY);
        box.getAccountTree(authToken, pathStack.getTopFolderId(), new String[] {Box.PARAM_ONELEVEL, Box.PARAM_NOFILES}, new GetAccountTreeListener() {

            @Override
            public void onComplete(BoxFolder boxFolder, String status) {
            	if (!status.equals(GetAccountTreeListener.STATUS_LISTING_OK)) {
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_error_listing_folders), Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }

                /**
                 * Box.getAccountTree() was successful. boxFolder contains a list of subfolders and files. Shove those into an array so that our list adapter
                 * displays them.
                 */

                items = new TreeListItem[boxFolder.getFoldersInFolder().size() + boxFolder.getFilesInFolder().size()];

                int i = 0;

                Iterator<? extends BoxFolder> foldersIterator = boxFolder.getFoldersInFolder().iterator();
                while (foldersIterator.hasNext()) {
                    BoxFolder subfolder = foldersIterator.next();
                    TreeListItem item = new TreeListItem();
                    item.id = subfolder.getId();
                    item.name = subfolder.getFolderName();
                    item.type = TreeListItem.TYPE_FOLDER;
                    item.folder = subfolder;
                    item.updated = subfolder.getUpdated();
                    items[i] = item;
                    i++;
                }

                Iterator<? extends BoxFile> filesIterator = boxFolder.getFilesInFolder().iterator();
                while (filesIterator.hasNext()) {
                    BoxFile boxFile = filesIterator.next();
                    TreeListItem item = new TreeListItem();
                    item.id = boxFile.getId();
                    item.name = boxFile.getFileName();
                    item.type = TreeListItem.TYPE_FILE;
                    item.file = boxFile;
                    item.updated = boxFile.getUpdated();
                    items[i] = item;
                    i++;
                }

                adapter.notifyDataSetChanged();
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onIOException(final IOException e) {
            	progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), getString(R.string.toast_failed_to_access_box_com) + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onListItemClick(ListView l, View v, final int position, long id) {
        pathStack.pushFolderAndId(items[position].name, items[position].id);
        pathTextView.setText(pathStack.getVerbosePath());
        refresh();
    }

    /**
     * Just a utility class to store BoxFile and BoxFolder objects, which can be passed as the source data of our list adapter.
     */
    private class TreeListItem {

        public static final int TYPE_FILE = 1;
        public static final int TYPE_FOLDER = 2;
        public int type;
        public long id;
        public String name;
        public BoxFile file;
        @SuppressWarnings("unused")
        public BoxFolder folder;
        public long updated;
    }

    private class MyArrayAdapter extends ArrayAdapter<TreeListItem> {

    	private LayoutInflater mInflater;
    	private Bitmap mIcon_folder;    	
        private final Context context;

        public MyArrayAdapter(Context context, int textViewResourceId, TreeListItem[] objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            mIcon_folder = BitmapFactory.decodeResource(context.getResources(),R.drawable.folder);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        	ViewHolder holder = null;
              if(convertView == null){
                convertView = mInflater.inflate(R.layout.list_items, null);
                holder = new ViewHolder();
                holder.f_title = ((TextView) convertView.findViewById(R.id.f_title));
                holder.f_icon = ((ImageView) convertView.findViewById(R.id.f_icon)) ;
                convertView.setTag(holder);
              }else{
                holder = (ViewHolder) convertView.getTag();
              }
              holder.f_title.setText(items[position].name);
              holder.f_icon.setImageBitmap(mIcon_folder);
            return convertView;
        }
        @Override
        public int getCount() {
            return items.length;
        }
        
        private class ViewHolder{
            TextView f_title;
            ImageView f_icon;
          }
    }
    
    private class PathStack {
    	Stack<String> folderStack = new Stack<String>();
    	Stack<Long> folderIdStack = new Stack<Long>();
    	public String getPath() {
    		String result = "";
    		for (int i = 1; i < folderStack.size(); i++) {
    			result += "/"+folderStack.get(i);
    		}
    		return result;
    	}
    	public int size() {
			return folderIdStack.size();
		}
		public long getTopFolderId() {
			return folderIdStack.peek();
		}
		public String getVerbosePath() {
    		String path = getPath();
			if (path.startsWith("/"))
    			return "Box:/"+getPath();
			else
				return "Box://"+getPath();
    	}
    	
    	public void pushFolderAndId(String folder, Long id) {
    		folderStack.push(folder);
    		folderIdStack.push(id);
    	}
    	
    	public void pop() {
    		folderStack.pop();
    		folderIdStack.pop();
    	}
    }
}
