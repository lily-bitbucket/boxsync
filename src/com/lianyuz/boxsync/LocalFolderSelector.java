package com.lianyuz.boxsync;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Stack;

import com.box.androidlib.Box;
import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;
import com.box.androidlib.ResponseListeners.GetAccountTreeListener;
import com.lianyuz.boxsync.core.SyncMng;

import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.text.Html;
import android.text.format.DateFormat;

public class LocalFolderSelector extends ListActivity  {

    private MyArrayAdapter adapter;
    private TreeListItem[] items;
    
    private PathStack pathStack;

    Button selectButton;
	Button cancelButton;
	Button upButton;
	Button createFolderButton;
	TextView pathTextView;
	ProgressBar progressBar;
	View myView;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder_selector);
 
        pathStack = new PathStack();
        
        pathTextView = (TextView) findViewById(R.id.folder_selector_path);
        pathTextView.setText(pathStack.getVerbosePath());
        selectButton = (Button) findViewById(R.id.button_folder_selector_select);
        selectButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent intent = new Intent();
            	intent.putExtra("local_sync_folder", pathStack.getPath());
                setResult(MainSyncStatus.LOCAL_SYNC_FOLDER_SELECT, intent);
                finish();
            }
        });
        cancelButton = (Button) findViewById(R.id.button_folder_selector_cancel);
        cancelButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        upButton = (Button) findViewById(R.id.button_folder_selector_up);
        upButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            	if (!pathStack.empty()) {
            		pathStack.pop();
            		pathTextView.setText(pathStack.getVerbosePath());
            		refresh();
            	}
            }
        });
        createFolderButton = (Button) findViewById(R.id.button_folder_selector_createFolder);
		createFolderButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				LayoutInflater factory = LayoutInflater
						.from(LocalFolderSelector.this);
				myView = factory
						.inflate(R.layout.pop_up_give_folder_name, null);
				final EditText fileNameEditText = (EditText) myView
						.findViewById(R.id.et_newfolderName);
				AlertDialog.Builder builder = new AlertDialog.Builder(
						LocalFolderSelector.this).setPositiveButton(getString(R.string.OK),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								String newFolderName = fileNameEditText
										.getText().toString();
								if (newFolderName != null && !newFolderName.equals("")) {
									File newFolder = new File(pathStack
											.getPath() + "/" + newFolderName);
									Log.d("create folder",
											newFolder.getAbsolutePath());
									newFolder.mkdir();
									Toast.makeText(getApplicationContext(), getString(R.string.toast_folder_created), Toast.LENGTH_SHORT).show();
									refresh();
								}
							}
						}).setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
				AlertDialog nameDialog = builder.create();
				nameDialog.setView(myView);
				nameDialog.show();
			}
		});
        progressBar = (ProgressBar) findViewById(R.id.folder_selector_progress);
        
        
        
        
        // Initialize list items and set adapter
        items = new TreeListItem[0];
        adapter = new MyArrayAdapter(this, 0, items);
        setListAdapter(adapter);
        // Go get the account tree
        refresh();
    }

    /**
     * Refresh the tree.
     */
    private void refresh() {
    	try {
			progressBar.setVisibility(View.VISIBLE);
			int n = 0;
			File folder = new File(pathStack.getPath());
			if ( folder != null && folder.listFiles() != null ) {
				for ( File f : folder.listFiles() ) {
					if ( f.isDirectory() ) {
						n++;
					}
				}
			}
			items = new TreeListItem[n];
			int i = 0;
			if ( folder != null && folder.listFiles() != null ) {
				for ( File f : folder.listFiles() ) {
					if ( f.isDirectory() ) {
						TreeListItem item = new TreeListItem();
				        item.name = f.getName();
				        items[i] = item;
				        i++;
					}
				}
			}
			adapter.notifyDataSetChanged();
			progressBar.setVisibility(View.INVISIBLE);

		} catch (Exception e) {
			progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(), getString(R.string.toast_failed_to_access_local_files), Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
    }

    @Override
    protected void onListItemClick(ListView l, View v, final int position, long id) {
        Intent i = new Intent(LocalFolderSelector.this, LocalFolderSelector.class);
        pathStack.pushFolder(items[position].name);
        pathTextView.setText(pathStack.getVerbosePath());
        refresh();
    }

    /**
     * Just a utility class to store BoxFile and BoxFolder objects, which can be passed as the source data of our list adapter.
     */
    private class TreeListItem {

        public static final int TYPE_FILE = 1;
        public static final int TYPE_FOLDER = 2;
        public int type;
        public long id;
        public String name;
        public BoxFile file;
        @SuppressWarnings("unused")
        public BoxFolder folder;
        public long updated;
    }

    private class MyArrayAdapter extends ArrayAdapter<TreeListItem> {

    	private LayoutInflater mInflater;
    	private Bitmap mIcon_folder;    	
        private final Context context;

        public MyArrayAdapter(Context context, int textViewResourceId, TreeListItem[] objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            mIcon_folder = BitmapFactory.decodeResource(context.getResources(),R.drawable.folder);
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        	ViewHolder holder = null;
              if(convertView == null){
                convertView = mInflater.inflate(R.layout.list_items, null);
                holder = new ViewHolder();
                holder.f_title = ((TextView) convertView.findViewById(R.id.f_title));
                holder.f_icon = ((ImageView) convertView.findViewById(R.id.f_icon)) ;
                convertView.setTag(holder);
              }else{
                holder = (ViewHolder) convertView.getTag();
              }
              holder.f_title.setText(items[position].name);
              holder.f_icon.setImageBitmap(mIcon_folder);
            return convertView;
        }
        @Override
        public int getCount() {
            return items.length;
        }
        
        private class ViewHolder{
            TextView f_title;
            ImageView f_icon;
          }
    }
    
    private class PathStack {
    	Stack<String> folderStack = new Stack<String>();
    	public String getPath() {
    		String result = "";
    		for (int i = 0; i < folderStack.size(); i++) {
    			result += "/"+folderStack.get(i);
    		}
    		if ( result.equals("") )
    			result += "/";
    		Log.d("path", result);
    		return result;
    	}
    	public String peek() {
			return folderStack.peek();
		}
		public void pushFolder(String name) {
			folderStack.push(name);
		}
		public String getVerbosePath() {
    		return "Local:/"+getPath();
    	}
    	
    	public void pop() {
    		folderStack.pop();
    	}
    	
    	public boolean empty() {
    		return folderStack.empty();
    	}
    }
}
