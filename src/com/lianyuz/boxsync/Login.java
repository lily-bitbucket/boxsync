package com.lianyuz.boxsync;

import com.box.androidlib.activities.BoxAuthentication;
import com.lianyuz.boxsync.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.widget.Toast;

public class Login extends Activity {

	private static final int AUTH_REQUEST_CODE = 100;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Intent intent = new Intent(this, BoxAuthentication.class);
        intent.putExtra("API_KEY", Constants.API_KEY); // API_KEY is required
        startActivityForResult(intent, AUTH_REQUEST_CODE);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTH_REQUEST_CODE) {
            if (resultCode == BoxAuthentication.AUTH_RESULT_SUCCESS) {
                // Store auth key in shared preferences.
                // BoxAuthentication activity will set the auth key into the
                // resulting intent extras, keyed as AUTH_TOKEN
                final SharedPreferences prefs = getSharedPreferences(Constants.PREFS_FILE_NAME, MODE_PRIVATE);
                final SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constants.PREFS_KEY_AUTH_TOKEN, data.getStringExtra("AUTH_TOKEN"));
                editor.commit();
                finish();
            } else if (resultCode == BoxAuthentication.AUTH_RESULT_FAIL) {
                Toast.makeText(getApplicationContext(), getString(R.string.unable_to_login), Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                finish();
            }
        }
    }
    
}
