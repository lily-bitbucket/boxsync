package com.lianyuz.boxsync;


import java.io.File;
import java.io.IOException;
import java.security.Policy;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.box.androidlib.Box;
import com.box.androidlib.DAO.User;
import com.box.androidlib.ResponseListeners.GetAccountInfoListener;
import com.lianyuz.boxsync.core.BoxSyncResult;
import com.lianyuz.boxsync.core.SyncListener;
import com.lianyuz.boxsync.core.SyncMng;
import com.lianyuz.boxsync.core.filters.SyncFilterMng;
import com.lianyuz.boxsync.core.policies.BasePolicy;
import com.lianyuz.boxsync.core.policies.LocalOnly;
import com.lianyuz.boxsync.core.policies.LocalPriority;
import com.lianyuz.boxsync.core.policies.RemoteOnly;
import com.lianyuz.boxsync.core.policies.RemotePriority;
import com.lianyuz.boxsync.core.util.TextUtil;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainSyncStatus extends Activity implements SyncListener {

	// Activity request codes
    static final int BOX_SYNC_FOLDER_SELECT = 1000;
    static final int LOCAL_SYNC_FOLDER_SELECT = 1001;

	
	// Menu button options
    private static final int MENU_ID_ABOUT = 1;
    private static final int MENU_ID_SETTINGS = 2;
    private static final int MENU_ID_LOGOUT = 3;
    
    // Preferences
    private static String LAST_SUCC_SYNC_DATE = "LAST_SUCC_SYNC_DATE";
    private static String LAST_SUCC_SYNC_TOTAL_SPACE = "LAST_SUCC_SYNC_TOTAL_SPACE";
    private static String LAST_SUCC_SYNC_USED_SPACE = "LAST_SUCC_SYNC_USED_SPACE";
    private static String LAST_SUCC_SYNC_LOCAL_FOLDER = "LAST_SUCC_SYNC_LOCAL_FOLDER";
    private static String LAST_SUCC_SYNC_BOX_FOLDER = "LAST_SUCC_SYNC_BOX_FOLDER";
    
    static String LAST_SYNC_LOCAL_FOLDER = "LAST_SYNC_LOCAL_FOLDER";
    static String LAST_SYNC_BOX_FOLDER = "LAST_SYNC_BOX_FOLDER";
    static String LAST_SYNC_BOX_FOLDER_ID = "LAST_SYNC_BOX_FOLDER_ID";
    static String SAVED_SYNC_BEHAVIOR = "SAVED_SYNC_BEHAVIOR";
    static int DEF_SYNC_BEHAVIOR = R.id.settings_rb_lp;
    static int DEF_SYNC_BEHAVIOR_INDEX = 0;



    private Handler handler;

    private String authToken;
    
    String localSyncFolder;
	String boxSyncFolder;
	Long boxSyncFolderId;
	SyncFilterMng syncFilterMng;
	BasePolicy policy;
    
    ProgressBar syncProgressBar;
    TextView lastSyncInfoTextView;
    EditText localSyncFolderEditText;
    EditText boxSyncFolderEditText;
	Button syncButton;
	Button selectBoxSyncFolderButton;
	Button selectLocalSyncFolderButton;
	Button syncSettingsButton;

	public Handler getHandler() {
		return handler;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_sync_status);
        
        
        
        final SharedPreferences prefs = getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
		authToken = prefs.getString(Constants.PREFS_KEY_AUTH_TOKEN, null);
		
		handler = new Handler();
        syncFilterMng = new SyncFilterMng();
		policy = createSyncPolicy(prefs.getInt(SAVED_SYNC_BEHAVIOR, DEF_SYNC_BEHAVIOR));
        
		final MainSyncStatus mss = this;
        syncButton = (Button) findViewById(R.id.button_sync);
        syncProgressBar = (ProgressBar) findViewById(R.id.pb_syncProgress);
        lastSyncInfoTextView = (TextView) findViewById(R.id.tv_lastSyncInfo);
        localSyncFolderEditText = (EditText) findViewById(R.id.localFolder);
        boxSyncFolderEditText = (EditText) findViewById(R.id.boxFolder);
        
        localSyncFolderEditText.setText(prefs.getString(LAST_SYNC_LOCAL_FOLDER, null));
        boxSyncFolderEditText.setText(prefs.getString(LAST_SYNC_BOX_FOLDER, null));

        prepareLastSuccSyncInfo();
        syncButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            		localSyncFolder = localSyncFolderEditText.getText().toString();
            		boxSyncFolder = boxSyncFolderEditText.getText().toString();
            		boxSyncFolderId = prefs.getLong(LAST_SYNC_BOX_FOLDER_ID, 0l);

            		if ( TextUtil.isLegalFolder(localSyncFolder) && TextUtil.isLegalFolder(boxSyncFolder) )
            		{
            			SyncMng syncMng = new SyncMng(authToken, localSyncFolder, boxSyncFolder, boxSyncFolderId, syncFilterMng, policy, mss);
            			syncMng.execute(new Object[0]);
            			
            	    	toggleProgressBar(syncProgressBar);
            		}
            		else
            			Toast.makeText(getApplicationContext(), getString(R.string.toast_cannot_sync_on_selected_paths),
                                Toast.LENGTH_SHORT).show();
               
            }
        });
        selectLocalSyncFolderButton = (Button) findViewById(R.id.button_selectLocalSyncFolder);
        selectLocalSyncFolderButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent intent = new Intent(MainSyncStatus.this, LocalFolderSelector.class);
            	startActivityForResult(intent, LOCAL_SYNC_FOLDER_SELECT);
            }
        });
        selectBoxSyncFolderButton = (Button) findViewById(R.id.button_selectBoxSyncFolder);
        selectBoxSyncFolderButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent intent = new Intent(MainSyncStatus.this, BoxFolderSelector.class);
            	startActivityForResult(intent, BOX_SYNC_FOLDER_SELECT);
            }
        });
        syncSettingsButton = (Button) findViewById(R.id.button_sync_options);
        syncSettingsButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
            	View myView;
            	LayoutInflater factory = LayoutInflater
						.from(MainSyncStatus.this);
				myView = factory
						.inflate(R.layout.sync_settings, null);
				myView.setPadding(5, 5, 5, 5);
				final RadioGroup syncBehaviorRadioGroup = (RadioGroup) myView
						.findViewById(R.id.settings_rg);
				final TextView info = (TextView) myView.findViewById(R.id.settings_tv_sync_behavior_info);
				final String[] behaviroInfo = getResources().getStringArray(R.array.sync_behavior_info);
				final String[] behaviroNames = getResources().getStringArray(R.array.sync_behavior_keys);
				for ( int i = 0; i < behaviroNames.length; i++ ) ((RadioButton)syncBehaviorRadioGroup.getChildAt(i)).setText(behaviroNames[i]);
				syncBehaviorRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		            @Override
		            public void onCheckedChanged(RadioGroup RG, int checkID) {
		            	
						switch(checkID) {
						case R.id.settings_rb_lp:
							info.setText(behaviroInfo[0]);
							break;
						case R.id.settings_rb_lo:
							info.setText(behaviroInfo[1]);
							break;
						case R.id.settings_rb_bp:
							info.setText(behaviroInfo[2]);
							break;
						case R.id.settings_rb_bo:
							info.setText(behaviroInfo[3]);
							break;
						}
		            }
				});
				AlertDialog.Builder builder = new AlertDialog.Builder(
						MainSyncStatus.this).setPositiveButton(getString(R.string.select),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								int checkID = syncBehaviorRadioGroup.getCheckedRadioButtonId();
								editor.putInt(SAVED_SYNC_BEHAVIOR, checkID);
								editor.commit();
								policy = createSyncPolicy(checkID);
							}
						}).setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						}).setTitle(getString(R.string.sync_behavior));
				syncBehaviorRadioGroup.check(prefs.getInt(SAVED_SYNC_BEHAVIOR, DEF_SYNC_BEHAVIOR));
				info.setText(behaviroInfo[DEF_SYNC_BEHAVIOR_INDEX]);
				AlertDialog nameDialog = builder.create();
				nameDialog.setView(myView);
				nameDialog.show();
            }
        });
    }

    private BasePolicy createSyncPolicy(int id) {
    	switch(id) {
		case R.id.settings_rb_lp:
			return new LocalPriority();
		case R.id.settings_rb_lo:
			return new LocalOnly();
		case R.id.settings_rb_bp:
			return new RemotePriority();
		case R.id.settings_rb_bo:
			return new RemoteOnly();
		}
		return null;
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (requestCode == BOX_SYNC_FOLDER_SELECT) {
    		if ( data != null ) {
    			boxSyncFolder = data.getStringExtra("box_sync_folder");
    			boxSyncFolderEditText.setText(boxSyncFolder);
    			final SharedPreferences prefs = getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
    			SharedPreferences.Editor editor = prefs.edit();
            	editor.putString(LAST_SYNC_BOX_FOLDER, boxSyncFolder);
            	editor.putLong(LAST_SYNC_BOX_FOLDER_ID, data.getLongExtra("box_sync_folder_id", 0l));
            	editor.commit();
    		}
    	} else if (requestCode == LOCAL_SYNC_FOLDER_SELECT) {
    		if ( data != null ) {
    			localSyncFolder = data.getStringExtra("local_sync_folder");  
    			localSyncFolderEditText.setText(localSyncFolder);
    			final SharedPreferences prefs = getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
    			SharedPreferences.Editor editor = prefs.edit();
    			editor.putString(LAST_SYNC_LOCAL_FOLDER, localSyncFolder);
    			editor.commit();
    			
    		}
    	}
    }
    
	public void onSyncFinished(BoxSyncResult bsr) {
		Log.d("onSyncFinished", "bsr.isSyncSucc(): "+bsr.isSyncSucc());
    	toggleProgressBar(syncProgressBar);
    	if (bsr.isSyncSucc()) {
    		updateLastSuccSyncInfo();
    	} else {
    		final MainSyncStatus mss = this;
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setMessage(Html.fromHtml(getString(R.string.sync_error)+" <p> 1. "+getString(R.string.network_conncetions)+" <p> 2. "+R.string.rw_permissions))
        	       .setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	           		toggleProgressBar(syncProgressBar);
        	        	   	SyncMng syncMng = new SyncMng(authToken, localSyncFolder, boxSyncFolder, boxSyncFolderId, syncFilterMng, policy, mss);
        	        	   	syncMng.execute(new Object[0]);
        	           }
        	       })
        	       .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	        	   dialog.cancel();
        	           }
        	       });
        	AlertDialog alert = builder.create();
        	alert.show();
    	}
	}
    
	private void updateLastSuccSyncInfo() {
		final SharedPreferences prefs = getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        editor.putString(LAST_SUCC_SYNC_LOCAL_FOLDER, localSyncFolder);
    	editor.putString(LAST_SUCC_SYNC_BOX_FOLDER, boxSyncFolder);
    	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		editor.putString(LAST_SUCC_SYNC_DATE, dateFormat.format(date));
		editor.commit();
		prepareLastSuccSyncInfo();
        
		final Box box = Box.getInstance(Constants.API_KEY);
		box.getAccountInfo(authToken, new GetAccountInfoListener() {
            @Override
            public void onComplete(final User boxUser, String status) {
            	Log.d("updateLastSuccSyncInfo", "got box account info");
                if (status.equals(GetAccountInfoListener.STATUS_GET_ACCOUNT_INFO_OK) && boxUser != null) {
                	Log.d("updateLastSuccSyncInfo", "updating");
                	NumberFormat nf = NumberFormat.getNumberInstance();
                    nf.setMaximumFractionDigits(2);
                	String totalSpace = ""+nf.format(1.0*boxUser.getSpaceAmount()/1024/1024/1024)+"GB";
                	String usedSpace = ""+nf.format(1.0*boxUser.getSpaceUsed()/1024/1024/1024)+"GB";
                	editor.putString(LAST_SUCC_SYNC_TOTAL_SPACE, totalSpace);
                	editor.putString(LAST_SUCC_SYNC_USED_SPACE, usedSpace);
                	editor.commit();
                	prepareLastSuccSyncInfo();
                } else {
                	Toast.makeText(getApplicationContext(), getString(R.string.sync_minor_problem),
                            Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onIOException(IOException e) {
            	Toast.makeText(getApplicationContext(), getString(R.string.sync_minor_problem),
                        Toast.LENGTH_LONG).show();
            }
        });
	}

	
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_ID_ABOUT, 0, getString(R.string.about));
        //menu.add(0, MENU_ID_SETTINGS, 1, "Settings");
        menu.add(0, MENU_ID_LOGOUT, 2, getString(R.string.log_out));
        return true;
    }
 
	@Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
		final SharedPreferences prefs = getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        switch (menuItem.getItemId()) {
            case MENU_ID_ABOUT:
            	AlertDialog.Builder builder = new AlertDialog.Builder(this);
            	builder.setMessage(getString(R.string.author_info)+": "+this.getString(R.string.version))
            	       .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
            	           public void onClick(DialogInterface dialog, int id) {
            	           }
            	       });
            	AlertDialog alert = builder.create();
            	alert.show();
            	break;
            case MENU_ID_LOGOUT:
            	editor.remove(Constants.PREFS_KEY_AUTH_TOKEN);
            	editor.commit();
            	authToken = null;
            	Intent intent = new Intent(this, Splash.class);
            	startActivity(intent);
            	finish();
            	break;
        }
        return false;
    }
	
	public void prepareLastSuccSyncInfo() {
		SharedPreferences prefs = getSharedPreferences(Constants.PREFS_FILE_NAME, Context.MODE_PRIVATE);
		String lastBoxSyncFolder = prefs.getString(LAST_SUCC_SYNC_BOX_FOLDER, "None");
		String lastLocalSyncFolder = prefs.getString(LAST_SUCC_SYNC_LOCAL_FOLDER, "None");
		String date = prefs.getString(LAST_SUCC_SYNC_DATE, "None");
		String totalSpace = prefs.getString(LAST_SUCC_SYNC_TOTAL_SPACE, "None");
		String usedSpace = prefs.getString(LAST_SUCC_SYNC_USED_SPACE, "None");
		
		String htmlText = "<b> "+getString(R.string.last_success_sync)+"</b><p>"+
		getString(R.string.date)+" "+date+"<br/>"+
		getString(R.string.local_folder)+" "+lastLocalSyncFolder+"<br/>"+
		getString(R.string.box_folder)+" "+lastBoxSyncFolder+"<br/>"+
		getString(R.string.box_space_total)+" "+totalSpace+"<br/>"+
		getString(R.string.box_space_used)+" "+usedSpace+"<br/>";
		
		lastSyncInfoTextView.setText(Html.fromHtml(htmlText));
	}
	
	private void toggleProgressBar(ProgressBar progressBar) {
	    switch (progressBar.getVisibility()) {
	    case View.GONE:
	        progressBar.setVisibility(View.VISIBLE);
	        break;
	    default:
	        progressBar.setVisibility(View.GONE);
	        break;
	    }
	}
}
