package com.lianyuz.boxsync.core;

import android.os.Handler;

public interface SyncListener {
	public void onSyncFinished(BoxSyncResult bsr);
	public Handler getHandler();
}
