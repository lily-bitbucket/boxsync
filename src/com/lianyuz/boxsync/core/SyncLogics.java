package com.lianyuz.boxsync.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.lianyuz.boxsync.core.file.LocalFileInfo;
import com.lianyuz.boxsync.core.file.BoxFileInfo;

import com.lianyuz.boxsync.core.policies.BasePolicy;

public class SyncLogics {
	
	/**
	 * Format: <RelativeFilePath, FileInfo>
	 */
	
	HashMap<String, LocalFileInfo> local;
	HashMap<String, BoxFileInfo> remote;
	
	String localSyncFolder;
	String remoteSyncFolder;
	
	public SyncLogics(HashSet<LocalFileInfo> localSet, String localSyncFolder, HashSet<BoxFileInfo> remoteSet, String remoteSyncFolder) {
		this.local = new HashMap<String, LocalFileInfo>();
		this.remote = new HashMap<String, BoxFileInfo>();
		this.localSyncFolder = localSyncFolder;
		this.remoteSyncFolder = remoteSyncFolder;
		for (LocalFileInfo aLocalFileInfo : localSet) {
			String relativePath = getRelativeFilePath(aLocalFileInfo.getFilePath(), localSyncFolder);
			this.local.put(relativePath, aLocalFileInfo);
		}
		for (BoxFileInfo aBoxFileInfo : remoteSet) {
			String relativePath = getRelativeFilePath(aBoxFileInfo.getFilePath(), remoteSyncFolder);
			this.remote.put(relativePath, aBoxFileInfo);
		}
		removeIdenticalEntries();
	}
	
	public void removeIdenticalEntries() {
		List<String> identicals = new ArrayList<String>();
		for (String aLocalRelativePath : local.keySet()) {
			if (remote.get(aLocalRelativePath) != null) {
				String localFileSha1 = local.get(aLocalRelativePath).getSha1();
				String remoteFileSha1 = remote.get(aLocalRelativePath).getSha1();
				if (localFileSha1.equals(remoteFileSha1)) {
					identicals.add(aLocalRelativePath);
					remote.remove(aLocalRelativePath);
				}
			}
		}
		for (String aLocalRelativePath : identicals) {
			local.remove(aLocalRelativePath);
		}
	}
	
	/**
	 * 
	 * @param policy
	 * @return return value are file path of remote format.
	 */
	public HashSet<BoxFileInfo> getCopyToLocalFileList(BasePolicy policy) {
		HashSet<BoxFileInfo> result = new HashSet<BoxFileInfo>();
		for (String fr : remote.keySet()) {
			LocalFileInfo info_fl = local.get(fr);
			BoxFileInfo info_fr = remote.get(fr);
			//missing local file
			if ( info_fl == null ) {
				if (policy.shouldCopyMissingFileToLocal()) {
					result.add(info_fr);
				}
			}
			// same file name, but different version
			else {
				if (policy.shouldOverwriteLocalConflictFile(info_fl, info_fr)) {
					result.add(info_fr);
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param policy
	 * @return return value are file path of local format.
	 */
	public HashSet<LocalFileInfo> getDeleteFromLocalFileList(BasePolicy policy) {
		HashSet<LocalFileInfo> result = new HashSet<LocalFileInfo>();
		for (String fl : local.keySet()) {
			BoxFileInfo info_fr = remote.get(fl);
			LocalFileInfo info_fl = local.get(fl);
			//missing remote file
			if ( info_fr == null ) {
				if (policy.shouldDeleteExtraFileFromLocal()) {
					result.add(info_fl);
				}
			}
			// same file name, but different version will be handled by copying action
		}
		return result;
	}
	
	/**
	 * 
	 * @param policy
	 * @return return value are file path of local format.
	 */
	public HashSet<LocalFileInfo> getCopyToRemoteFileList(BasePolicy policy) {
		HashSet<LocalFileInfo> result = new HashSet<LocalFileInfo>();
		for (String fl : local.keySet()) {
			LocalFileInfo info_fl = local.get(fl);
			BoxFileInfo info_fr = remote.get(fl);
			//missing local file
			if ( info_fr == null ) {
				if (policy.shouldCopyMissingFileToRemote()) {
					result.add(info_fl);
				}
			}
			// same file name, but different version
			else {
				if (policy.shouldOverwriteRemoteConflictFile(info_fl, info_fr)) {
					result.add(info_fl);
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param policy
	 * @return return value are file path of remote format.
	 */
	public HashSet<BoxFileInfo> getDeleteFromRemoteFileList(BasePolicy policy) {
		HashSet<BoxFileInfo> result = new HashSet<BoxFileInfo>();
		for (String fr : remote.keySet()) {
			BoxFileInfo info_fr = remote.get(fr);
			LocalFileInfo info_fl = local.get(fr);
			//missing remote file
			if ( info_fl == null ) {
				if (policy.shouldDeleteExtraFileFromRemote()) {
					result.add(info_fr);
				}
			}
			// same file name, but different version will be handled by copying action
		}
		return result;
	}
	/**
	 * 
	 * @param relativeTo if path was "/aaa/bbb/c.txt", relativeTo is "/aaa", then result is "bbb/c.txt"
	 * @return
	 */
	public static String getRelativeFilePath(String filePath, String relativeTo) {
		if (!relativeTo.endsWith("/")) 
			relativeTo += "/";
		return filePath.substring(relativeTo.length());
	}
}
