package com.lianyuz.boxsync.core;

import java.util.HashSet;
import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.box.androidlib.Box;
import com.box.androidlib.DAO.BoxFolder;
import com.lianyuz.boxsync.Constants;
import com.lianyuz.boxsync.MainSyncStatus;
import com.lianyuz.boxsync.core.file.BoxFileInfo;
import com.lianyuz.boxsync.core.file.FileAccessUtil;
import com.lianyuz.boxsync.core.file.LocalFileInfo;
import com.lianyuz.boxsync.core.filters.SyncFilterMng;
import com.lianyuz.boxsync.core.policies.BasePolicy;
import com.lianyuz.boxsync.core.policies.LocalPriority;
import com.lianyuz.boxsync.core.policies.RemotePriority;

public class SyncMng extends AsyncTask {

	private String authToken;
	private SyncListener listener;
	private String localSyncFolder;
	private String boxSyncFolder;
	private Long boxSyncFolderId;
	private SyncFilterMng filterMng;
	private BasePolicy policy;
	
	BoxSyncResult syncResult = new BoxSyncResult();

	public SyncMng(String authToken, String localSyncFolder, String boxSyncFolder, Long boxSyncFolderId, SyncFilterMng filterMng, BasePolicy policy, SyncListener listener) {
		this.authToken = authToken;
		this.listener = listener;
		this.localSyncFolder = localSyncFolder;
		this.boxSyncFolder = boxSyncFolder;
		this.boxSyncFolderId = boxSyncFolderId;
		this.filterMng = filterMng;
		this.policy = policy;
	}
	
	/*private void tryDummySync() {
		try {
			BoxSyncResult syncResult = new BoxSyncResult();
			Random rand = new Random();
			syncResult.isSucc = rand.nextBoolean();
			Log.d("SyncMng", "calling back listener");
			listener.onSyncFinished(syncResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	private void trySync() {
		try {
			Log.d("MainSyncStatus", "trySync");
			final Box box = Box.getInstance(Constants.API_KEY);
			BoxFolder boxFolder = FileAccessUtil.getBoxFolder(box, authToken, 0l, new String[]{});
			if ( boxFolder != null )
				doSync(box, authToken, boxFolder, boxSyncFolderId);
			else {
				syncResult.isSucc = false;
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			syncResult.isSucc = false;
		}
		
		
	}

	private void doSync(Box box, String authToken, BoxFolder rootBoxFolder, Long boxSyncFolderId) {
		HashSet<LocalFileInfo> localFileSet = FileAccessUtil.getLocalFileSet(filterMng, localSyncFolder);
		HashSet<BoxFileInfo> remoteFileSet = FileAccessUtil.getRemoteFileSet(filterMng, rootBoxFolder, boxSyncFolderId);
		Log.d("doSync", remoteFileSet+","+localFileSet);
		
		if (remoteFileSet != null && localFileSet != null) {
			Log.d("localFileSet", localFileSet.toString());
			Log.d("remoteFileSet", remoteFileSet.toString());

			SyncLogics syncLogics = new SyncLogics(localFileSet, localSyncFolder, remoteFileSet, boxSyncFolder);
			
			boolean resultC2R = true, resultC2L = true, resultDFR = true, resultDFL = true;
			
			HashSet<LocalFileInfo> copyToRemoteFileList = syncLogics.getCopyToRemoteFileList(policy);
			if ( copyToRemoteFileList.size() > 0 ) {
				Log.d("copyToRemoteFileList", copyToRemoteFileList.toString());
				resultC2R = FileAccessUtil.copyToRemote(box, authToken, rootBoxFolder, boxSyncFolder, localSyncFolder, boxSyncFolderId, copyToRemoteFileList, listener.getHandler());
			}
			
			HashSet<BoxFileInfo> copyToLocalFileList = syncLogics.getCopyToLocalFileList(policy);
			if ( copyToLocalFileList.size() > 0 ) {
				Log.d("copyToLocalFileList", copyToLocalFileList.toString());
				resultC2L = FileAccessUtil.copyToLocal(authToken, boxSyncFolder, localSyncFolder, copyToLocalFileList, listener.getHandler());
			}
			
			HashSet<BoxFileInfo> deleteFromRemoteFileList = syncLogics.getDeleteFromRemoteFileList(policy);
			if ( deleteFromRemoteFileList.size() > 0 ) {
				Log.d("deleteFromRemoteFileList", deleteFromRemoteFileList.toString());
				resultDFR = FileAccessUtil.deleteFromRemote(box, authToken, boxSyncFolder, localSyncFolder, deleteFromRemoteFileList, localFileSet);
			}
			
			HashSet<LocalFileInfo> deleteFromLocalFileList = syncLogics.getDeleteFromLocalFileList(policy);
			if ( deleteFromLocalFileList.size() > 0 ) {
				Log.d("deleteFromLocalFileList", deleteFromLocalFileList.toString());
				resultDFL = FileAccessUtil.deleteFromLocal(deleteFromLocalFileList, remoteFileSet, boxSyncFolder, localSyncFolder);
			}
			
			syncResult.isSucc = resultDFL && resultDFR && resultC2L && resultC2R;
		}
		else
			syncResult.isSucc = false;
	}

	@Override
	protected Object doInBackground(Object... params) {
		Log.d("SyncMng", "running");
		//tryDummySync();
		trySync();
		return null;
	}
	
	@Override
    protected void onPostExecute(Object result) {
		listener.onSyncFinished(syncResult);
    }
	
}
