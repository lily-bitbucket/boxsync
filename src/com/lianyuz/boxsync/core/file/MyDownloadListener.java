package com.lianyuz.boxsync.core.file;

import java.io.IOException;

import com.box.androidlib.ResponseListeners.FileDownloadListener;

public class MyDownloadListener implements FileDownloadListener {
	@Override
    public void onComplete(final String status) {
    }

    @Override
    public void onIOException(final IOException e) {
        e.printStackTrace();
    }

    @Override
    public void onProgress(final long bytesDownloaded) {
    }
}
