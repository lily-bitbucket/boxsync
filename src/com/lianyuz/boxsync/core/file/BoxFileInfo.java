package com.lianyuz.boxsync.core.file;

public class BoxFileInfo implements FileInfo {
	public String fileName;
	public long fileId;
	public String parentFolderPath;
	public long parentFolderId;
	public long timeLastModified;
	public String sha1;

	public BoxFileInfo(String fileName, long fileId, String parentFolderPath, long parentFolderId, long timeLastModified, String sha1) {
		this.timeLastModified = timeLastModified;
		this.fileId = fileId;
		this.fileName = fileName;
		this.parentFolderId = parentFolderId;
		this.parentFolderPath = parentFolderPath;
		this.sha1 = sha1;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public String getSha1() {
		return sha1;
	}
	
	public long getFileId() {
		return fileId;
	}
	
	public String getParentFolderPath() {
		return parentFolderPath;
	}
	
	public long getParentFolderId() {
		return parentFolderId;
	}
	
	public String getFilePath() {
		String path = "";
		if ( !parentFolderPath.endsWith("/") )
			path += parentFolderPath+"/";
		path += fileName;
		return path;
	}
	
	public long getTimeLastModified() {
		return timeLastModified;
	}
	
	public boolean equals(BoxFileInfo fi) {
		return fi.timeLastModified == this.timeLastModified
				&& fi.fileId == this.fileId
				&& fi.fileName.equals(this.fileName)
				&& fi.parentFolderId == this.parentFolderId
				&& fi.sha1.equals(this.sha1)
				&& fi.parentFolderPath.equals(this.parentFolderPath);
	}

	public boolean isMoreRecentlyModifiedThan(FileInfo another) {
		return this.getTimeLastModified() > another.getTimeLastModified();
	}

	public String toString() {
		return ""
				+ fileId+", "
				+ fileName+", "
				+ parentFolderId+", "
				+ parentFolderPath+", "
				+ timeLastModified+", "
				+ sha1
				;
	}
}
