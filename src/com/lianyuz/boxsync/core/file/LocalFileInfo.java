package com.lianyuz.boxsync.core.file;

public class LocalFileInfo implements FileInfo {
	public String fileName;
	public String parentFolderPath;
	public long timeLastModified;
	public String sha1;

	public LocalFileInfo(String fileName, String parentFolderPath, long timeLastModified, String sha1) {
		this.timeLastModified = timeLastModified;
		this.fileName = fileName;
		this.parentFolderPath = parentFolderPath;
		this.sha1 = sha1 ;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public String getSha1() {
		return sha1;
	}
	
	public String getParentFolderPath() {
		return parentFolderPath;
	}
	
	public String getFilePath() {
		String path = "";
		if ( !parentFolderPath.endsWith("/") )
			path += parentFolderPath+"/";
		path += fileName;
		return path;
	}

	public long getTimeLastModified() {
		return timeLastModified;
	}
	
	public boolean equals(BoxFileInfo fi) {
		return fi.timeLastModified == this.timeLastModified
				&& fi.fileName.equals(this.fileName)
				&& fi.sha1.equals(this.sha1)
				&& fi.parentFolderPath.equals(this.parentFolderPath);
	}
	
	
	public boolean isMoreRecentlyModifiedThan(FileInfo another) {
		return this.getTimeLastModified() > another.getTimeLastModified();
	}
	
	public String toString() {
		return ""
				+ fileName+", "
				+ parentFolderPath+", "
				+ timeLastModified+", "
				+ sha1;
	}
}
