package com.lianyuz.boxsync.core.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.box.androidlib.Box;
import com.box.androidlib.BoxSynchronous;
import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;
import com.box.androidlib.FileTransfer.BoxFileUpload;
import com.box.androidlib.ResponseListeners.CreateFolderListener;
import com.box.androidlib.ResponseListeners.DeleteListener;
import com.box.androidlib.ResponseListeners.FileDownloadListener;
import com.box.androidlib.ResponseListeners.FileUploadListener;
import com.box.androidlib.ResponseListeners.GetAccountTreeListener;
import com.box.androidlib.ResponseParsers.AccountTreeResponseParser;
import com.box.androidlib.ResponseParsers.DefaultResponseParser;
import com.box.androidlib.ResponseParsers.FileResponseParser;
import com.box.androidlib.ResponseParsers.FolderResponseParser;
import com.box.androidlib.Utils.Cancelable;
import com.lianyuz.boxsync.Constants;
import com.lianyuz.boxsync.core.SyncLogics;
import com.lianyuz.boxsync.core.filters.SyncFilterMng;
import com.lianyuz.boxsync.core.policies.BasePolicy;

public class FileAccessUtil {
	public static HashSet<BoxFileInfo> getRemoteFileSet(SyncFilterMng filter, BoxFolder rootFolder, long remoteSyncFolderId) {
		//for (BoxFolder bf : boxFolder.getFoldersInFolder())
		//	Log.d("", bf.getFolderName()+", "+bf.getId());
		//////////
		Log.d("rootFolder", rootFolder.getFolderName());
		HashSet<BoxFileInfo> result = new HashSet<BoxFileInfo>();
		StringBuilder remoteSyncFolderPath = new StringBuilder();
		BoxFolder remoteSyncFolder = getRemoteFolderById(rootFolder, remoteSyncFolderId, remoteSyncFolderPath);
		if ( remoteSyncFolder != null ) {
			Log.d("FileAccessUtil", remoteSyncFolder.getFolderName()+"");
			generateBoxFileSet(filter, remoteSyncFolder, result, remoteSyncFolderPath);
			return result;
		}
		else
			return null;
	}
	
	private static BoxFolder getRemoteFolderById(BoxFolder aParentFolder, long remoteSyncFolderId, StringBuilder path) {
		List<? extends BoxFolder> folders = aParentFolder.getFoldersInFolder();
		for (BoxFolder bfd : folders) {
			if (bfd.getId() == remoteSyncFolderId) {
				path.append("/"+bfd.getFolderName());
				return bfd;
			}
		}
		for (BoxFolder bfd : folders) {
			StringBuilder temp = new StringBuilder(path);
			path.append("/"+bfd.getFolderName());
			BoxFolder result = getRemoteFolderById(bfd, remoteSyncFolderId, path);
			if ( result != null) 
				return result;
			else
			{
				path.delete(0, path.length());
				path.append(temp.toString());
			}
		}
		return null;
	}
	
	private static void generateBoxFileSet(SyncFilterMng filter, BoxFolder boxFolder, HashSet<BoxFileInfo> set, StringBuilder path) {
		List<? extends BoxFile> files = boxFolder.getFilesInFolder();
		for (BoxFile bf : files) {
			String fileName = bf.getFileName();
			String filePath = bf.getFolder().getPath()+fileName;
			if (!filter.isFiltered(fileName))
			{
				BoxFileInfo info = new BoxFileInfo(bf.getFileName(), bf.getId(), path.toString(), boxFolder.getId(), bf.getCreated()*1000, bf.getSha1());
				set.add(info);
			}
		}
		List<? extends BoxFolder> folders = boxFolder.getFoldersInFolder();
		for (BoxFolder bfd : folders) {
			String folderName = bfd.getFolderName();
			if (!filter.isFiltered(folderName))
			{
				StringBuilder temp = new StringBuilder(path);
				path.append("/"+bfd.getFolderName());
				generateBoxFileSet(filter, bfd, set, path);
				path.delete(0, path.length());
				path.append(temp.toString());
			}
		}
	}
	
	public static HashSet<LocalFileInfo> getLocalFileSet(SyncFilterMng filter, String syncFolder) {
		HashSet<LocalFileInfo> result = new HashSet<LocalFileInfo>();
		generateLocalFileSet(filter, syncFolder, result);
		return result;
	}
	
	private static void generateLocalFileSet(SyncFilterMng filter, String folder, HashSet<LocalFileInfo> fileSet) {
		File fd = new File(folder);
		for (File file : fd.listFiles()) {
			if ( !file.isDirectory()) {
				String fileName = file.getName();
				if (!filter.isFiltered(fileName))
				{
					LocalFileInfo info = new LocalFileInfo(file.getName(), file.getParent(), file.lastModified(), getSha1(file));
					fileSet.add(info);
				}
			}
			else {
				String folderName = file.getName();
				String folderPath = file.getPath();
				if (!filter.isFiltered(folderName))
				{
					generateLocalFileSet(filter, folderPath, fileSet);
				}
			}
		}
	}
	
	public static void createNecessaryFoldersOnRemote(Box box, String authToken, BoxFolder rootFolder, HashSet<LocalFileInfo> copyToRemoteFileList, String localSyncFolder, String remoteSyncFolder) {
		ArrayList<String> foldersToCreate = new ArrayList<String>();
		for (LocalFileInfo aLocalFileInfo : copyToRemoteFileList) {
			String parentFolderPath = aLocalFileInfo.getParentFolderPath();
			String remoteParentFolderPath = getRemoteFilePathByLocalFilePath(parentFolderPath, localSyncFolder, remoteSyncFolder);
			if (remoteParentFolderPath.endsWith("/"))
				remoteParentFolderPath = remoteParentFolderPath.substring(0, remoteParentFolderPath.length()-1);
			String[] ss = remoteParentFolderPath.split("/");
			String intermediateFolder = "";
			for (int i = 0; i < ss.length; i++) {
				if ( !ss[i].equals("") ) {
					intermediateFolder += "/"+ss[i];
					if (!foldersToCreate.contains(intermediateFolder)) {
						foldersToCreate.add(intermediateFolder);
					}
				}
			}
		}
		Collections.sort(foldersToCreate, new Comparator<String>(){
			public int compare(String a, String b) {
				Integer i = a.split("/").length;
				Integer j = b.split("/").length;
				return i.compareTo(j);
			}
			
		});
		for (String aFolderPath : foldersToCreate) {
			rootFolder = getBoxFolder(box, authToken, rootFolder.getId(), new String[]{});
			boolean result = createFolderRecursively(box, authToken, rootFolder, aFolderPath, new StringBuilder());
			Log.d("createNecessaryFoldersOnRemote: ", aFolderPath+", created: "+result);
		}
	}

	
	/**
	 * This is async mode
	 */
	public static boolean createFolderRecursively(Box box, String authToken, BoxFolder rootFolder, String folderPathToCreate, StringBuilder pathHelper) {
		//Log.d("pathHelper: ", pathHelper.toString());
		List<? extends BoxFolder> folders = rootFolder.getFoldersInFolder();
		for (BoxFolder bfd : folders) {
			Log.d("createFolderRecursively:", bfd.getFolderName());
			StringBuilder temp = new StringBuilder(pathHelper.toString());
			//////
			String folderName = getLastFolderNameFromPath(folderPathToCreate);
			if (folderPathToCreate.equals(pathHelper+"/"+bfd.getFolderName()+"/"+folderName)) {
				return createFolder(box, authToken, bfd.getId(), folderName, false);
			}
			//////
			pathHelper.append("/"+bfd.getFolderName());
			boolean result;
			if ((result = createFolderRecursively(box, authToken, bfd, folderPathToCreate, pathHelper)) == true)
				return result;
			else{
				pathHelper.delete(0, pathHelper.length());
				pathHelper.append(temp.toString());
			}
		}
		return false;
	}
	
	/**
	 * This is async mode
	 */
	public static boolean createFolder(Box box, String authToken, Long parentFolderId, String folderName, boolean share) {
		try {
			final FolderResponseParser response = BoxSynchronous.getInstance(box.getApiKey()).createFolder(authToken, parentFolderId, folderName, share);
			if (response.getStatus().equals(CreateFolderListener.STATUS_CREATE_OK) 
					|| response.getStatus().equals(CreateFolderListener.STATUS_S_FOLDER_EXISTS))
			{
				Log.d("createFolder: ", folderName+", "+response.getStatus());
				return true;
			}
			else
			{
				Log.d("createFolder: ", folderName+", "+response.getStatus());
				return false;
			}
		} catch (IOException e) {
			Log.d("createFolder: ", "IOException");
			return false;
		}
	}
	
	public static String getParentFolderFromPath(String filePath) {
		File f = new File(filePath);
		return f.getParent();
	}
	
	public static String getLastFolderNameFromPath(String filePath) {
		File f = new File(filePath);
		return f.getName();
	}

	public static HashMap<String, Long> genRemoteFolderAndIdMap(HashSet<BoxFileInfo> remoteFileSet) {
		HashMap<String, Long> result = new HashMap<String, Long>();
		for (BoxFileInfo info : remoteFileSet) {
			result.put(info.getParentFolderPath(), info.getParentFolderId());
		}
		return result;
	}

	public static BoxFolder getBoxFolder(Box box, String authToken, Long remoteSyncFolderId, String[] params) {
		try {
			final AccountTreeResponseParser response = BoxSynchronous.getInstance(box.getApiKey()).getAccountTree(authToken, remoteSyncFolderId, new String[]{});
			if (response.getStatus().equals(GetAccountTreeListener.STATUS_LISTING_OK))
			{
				Log.d("AccountTreeResponseParser", "complete");
				return response.getFolder();
			}
			else
				return null;
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String getLocalFilePathByRemoteFilePath(String remoteFilePath, String localSyncFolder, String remoteSyncFolder) {
		return localSyncFolder + remoteFilePath.substring(remoteSyncFolder.length(), remoteFilePath.length());
	}
	
	public static String getRemoteFilePathByLocalFilePath(String localFilePath, String localSyncFolder, String remoteSyncFolder) {
		return remoteSyncFolder + localFilePath.substring(localSyncFolder.length(), localFilePath.length());
	}

	public static Long findRemoteFolderId(String toFind,
			BoxFolder root, StringBuilder pathHelper) {
		for (BoxFolder bf : root.getFoldersInFolder()) {
			if (toFind.equals(pathHelper.toString()+"/"+bf.getFolderName())) {
				return bf.getId();
			}
			StringBuilder temp = new StringBuilder(pathHelper);
			Long result;
			if ((result = findRemoteFolderId(toFind, bf, pathHelper.append("/"+bf.getFolderName()))) != null )
				return result;
			else
			{
				pathHelper.delete(0, pathHelper.length());
				pathHelper.append(temp.toString());
			}
		}
		return null;
	}
	
	public static boolean copyToRemote(
			Box 					box, 
			String 					authToken, 
			BoxFolder 				rootBoxFolder, 
			String 					boxSyncFolder, 
			String 					localSyncFolder, 
			Long 					boxSyncFolderId, 
			HashSet<LocalFileInfo> 	copyToRemoteFileList,
			Handler					handler
			) {
		FileAccessUtil.createNecessaryFoldersOnRemote(box, authToken, rootBoxFolder, copyToRemoteFileList, localSyncFolder, boxSyncFolder);
		
		BoxFolder rootBoxFolderRefreshed = FileAccessUtil.getBoxFolder(box, authToken, rootBoxFolder.getId(), new String[]{});
		for (LocalFileInfo aLocalFileInfo : copyToRemoteFileList) {
			String localFilePath = aLocalFileInfo.getFilePath();
			String boxFilePath = FileAccessUtil.getRemoteFilePathByLocalFilePath(localFilePath, localSyncFolder, boxSyncFolder);
			String boxFolderPath = FileAccessUtil.getParentFolderFromPath(boxFilePath);
			Long remoteFolderId = FileAccessUtil.findRemoteFolderId(boxFolderPath, rootBoxFolderRefreshed, new StringBuilder());
			if (remoteFolderId != null)
			{
				 File localFile = new File(localFilePath);
				 try {
					 final FileResponseParser response = BoxSynchronous.getInstance(Constants.API_KEY).upload(authToken, Box.UPLOAD_ACTION_UPLOAD, new FileInputStream(localFile), localFile.getName(),
							 remoteFolderId, new MyUploadListener(), handler);
					if ( !response.getStatus().equals(FileUploadListener.STATUS_UPLOAD_OK) )
						 return false;
					Log.d("copyToRemote", aLocalFileInfo.getFilePath());
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
				 
			}
		}
		return true;
	}
	
	
	private static void deleteExtraFoldersOnRemote(Box box, String authToken,
			HashSet<BoxFileInfo> deleteFromRemoteFileList,
			HashSet<LocalFileInfo> localFileSet,
			String localSyncFolder, String boxSyncFolder) {
		for (BoxFileInfo boxInfo : deleteFromRemoteFileList) {
			String remoteFolderPath = boxInfo.getParentFolderPath();
			String genLocalFolderPath = FileAccessUtil.getLocalFilePathByRemoteFilePath(remoteFolderPath, localSyncFolder, boxSyncFolder);
			
			boolean shouldKeep = false;
			//find such a path in localFileSet. If so, we should keep this folder
			for (LocalFileInfo localInfo : localFileSet) {
				if (localInfo.getParentFolderPath().equals(genLocalFolderPath))
					shouldKeep = true;
			}
			//if it is the remote sync root folder, we don't delete it
			if (remoteFolderPath.equals(boxSyncFolder))
				shouldKeep = true;
			//////////////////////////////////
			if ( shouldKeep == false ) {
				try {
						Long parentId = boxInfo.getParentFolderId();
						BoxFolder boxFolder = FileAccessUtil.getBoxFolder(box, authToken, parentId, new String[]{});
						if ( boxFolder != null && boxFolder.getFileCount() == 0 ) {
							final String status = BoxSynchronous.getInstance(Constants.API_KEY).delete(authToken, Box.TYPE_FOLDER, parentId);
						}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
	}
	
	public static boolean deleteFromRemote(
			Box 					box, 
			String 					authToken, 
			String 					boxSyncFolder, 
			String 					localSyncFolder, 
			HashSet<BoxFileInfo> 	deleteFromRemoteFileList, 
			HashSet<LocalFileInfo>	localFileSet
			) {
		for (BoxFileInfo aBoxFileInfo : deleteFromRemoteFileList) {
			try {
				 final String status = BoxSynchronous.getInstance(Constants.API_KEY).delete(authToken, Box.TYPE_FILE, aBoxFileInfo.getFileId());
				 if (!status.equals(DeleteListener.STATUS_S_DELETE_NODE))
					 return false;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		FileAccessUtil.deleteExtraFoldersOnRemote(box, authToken, deleteFromRemoteFileList, localFileSet, localSyncFolder, boxSyncFolder);
		return true;
	}
	
	public static boolean copyToLocal(
			String 					authToken, 
			String boxSyncFolder,
			String localSyncFolder,
			HashSet<BoxFileInfo> 	copyToLocalFileList, Handler handler
			) {
		for (BoxFileInfo aBoxFileInfo : copyToLocalFileList) {
			String boxFilePath = aBoxFileInfo.getFilePath();
			Log.d("copyToLocal", "boxFilePath: "+boxFilePath);
			String localFilePath = FileAccessUtil.getLocalFilePathByRemoteFilePath(boxFilePath, localSyncFolder, boxSyncFolder);
			Log.d("copyToLocal", "generated localFilePath: "+localFilePath);
			String localFileFoder = getParentFolderFromPath(localFilePath);
			File localFolder = new File(localFileFoder);
			localFolder.mkdirs();
			DefaultResponseParser response;
			try {
				File localFile = new File(localFilePath);
				if ( !localFile.exists() )
					localFile.createNewFile();
				response = BoxSynchronous.getInstance(Constants.API_KEY).download(authToken, aBoxFileInfo.getFileId(), new FileOutputStream(localFile), null,
				        new MyDownloadListener(), handler);
				Log.d("copyToLocal", "response: "+response.getStatus());
				if ( !response.getStatus().equals(FileDownloadListener.STATUS_DOWNLOAD_OK))
					return false;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			
		}
		return true;
	}
	
	public static boolean deleteFromLocal(
			HashSet<LocalFileInfo> 	deleteFromLocalFileList,
			HashSet<BoxFileInfo>	remoteFileSet,
			String boxSyncFolder,
			String localSyncFolder
			) {
		for (LocalFileInfo aLocalFileInfo : deleteFromLocalFileList) {
			try {
				File localFile = new File(aLocalFileInfo.getFilePath());
				if (localFile.exists())
					localFile.delete();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		FileAccessUtil.deleteExtraFoldersOnLocal(remoteFileSet, deleteFromLocalFileList, localSyncFolder, boxSyncFolder);
		return true;
	}
	
	private static boolean deleteExtraFoldersOnLocal(
			HashSet<BoxFileInfo> remoteFileSet,
			HashSet<LocalFileInfo> 	deleteFromLocalFileList,
			String localSyncFolder, String boxSyncFolder
			) {
		for (LocalFileInfo localInfo : deleteFromLocalFileList) {
			String localFolderPath = localInfo.getParentFolderPath();
			String genRemoteFolderPath = FileAccessUtil.getLocalFilePathByRemoteFilePath(localFolderPath, localSyncFolder, boxSyncFolder);
			//find such a path in remoteFileSet, to decide whether we should keep this folder
			boolean shouldKeep = false;
			for (BoxFileInfo remoteInfo : remoteFileSet) {
				if (remoteInfo.getParentFolderPath().equals(genRemoteFolderPath))
					shouldKeep = true;
			}
			//if it is the local sync root folder, we don't delete it
			if (localFolderPath.equals(localSyncFolder))
				shouldKeep = true;
			//////////////////////////////////
			if ( shouldKeep == false ) {
				try {
					 File folder = new File(localFolderPath);
					 if ( folder.exists() && folder.listFiles().length == 0 )
						 folder.delete();
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		return true;
	}
	
	public static String getSha1(File file) {
		try {
			FileInputStream in = new FileInputStream(file);
			MessageDigest messagedigest;
			messagedigest = MessageDigest.getInstance("SHA-1");

			byte[] buffer = new byte[1024 * 1024 * 10];
			int len = 0;

			while ((len = in.read(buffer)) > 0) {
				messagedigest.update(buffer, 0, len);
			}

			return byte2hex(messagedigest.digest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String byte2hex(byte[] b) {
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
			if (stmp.length() == 1)
				hs = hs + "0" + stmp;
			else
				hs = hs + stmp;
		}
		return hs.toLowerCase();
	}
}
