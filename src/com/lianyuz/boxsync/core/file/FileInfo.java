package com.lianyuz.boxsync.core.file;

public interface FileInfo {
	public boolean isMoreRecentlyModifiedThan(FileInfo another);
	public long getTimeLastModified();
}
