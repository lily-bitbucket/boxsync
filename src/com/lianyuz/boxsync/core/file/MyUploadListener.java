package com.lianyuz.boxsync.core.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.widget.Toast;

import com.box.androidlib.Box;
import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.ResponseListeners.FileUploadListener;
import com.box.androidlib.Utils.Cancelable;

public class MyUploadListener implements FileUploadListener {

	@Override
	public void onComplete(BoxFile file, final String status) {
	}

	@Override
	public void onIOException(final IOException e) {
		e.printStackTrace();
	}

	@Override
	public void onMalformedURLException(final MalformedURLException e) {
		e.printStackTrace();
	}

	@Override
	public void onFileNotFoundException(final FileNotFoundException e) {
		e.printStackTrace();
	}

	@Override
	public void onProgress(final long bytesUploaded) {
	}
}
