package com.lianyuz.boxsync.core.policies;

import com.lianyuz.boxsync.core.file.FileInfo;

	public class LocalPriority implements BasePolicy {
	@Override
	public boolean shouldCopyMissingFileToLocal() {
		return false;
	}

	@Override
	public boolean shouldOverwriteLocalConflictFile(FileInfo local,
			FileInfo remote) {
		return false;
	}

	@Override
	public boolean shouldDeleteExtraFileFromLocal() {
		return false;
	}

	/*@Override
	public boolean shouldDeleteLocalConflictFile(FileInfo local,
			FileInfo remote) {
		return false;
	}*/

	@Override
	public boolean shouldCopyMissingFileToRemote() {
		return true;
	}

	@Override
	public boolean shouldOverwriteRemoteConflictFile(FileInfo local,
			FileInfo remote) {
		return true;
	}

	@Override
	public boolean shouldDeleteExtraFileFromRemote() {
		return false;
	}

	/*@Override
	public boolean shouldDeleteRemoteConflictFile(FileInfo local,
			FileInfo remote) {
		return true;
	}*/
}
