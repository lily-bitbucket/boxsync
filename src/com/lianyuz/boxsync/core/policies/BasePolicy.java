package com.lianyuz.boxsync.core.policies;

import com.lianyuz.boxsync.core.file.FileInfo;

public interface BasePolicy {

	boolean shouldCopyMissingFileToLocal();

	boolean shouldOverwriteLocalConflictFile(FileInfo local, FileInfo remote);

	boolean shouldDeleteExtraFileFromLocal();

	//boolean shouldDeleteLocalConflictFile(FileInfo local, FileInfo remote);

	boolean shouldCopyMissingFileToRemote();

	boolean shouldOverwriteRemoteConflictFile(FileInfo local, FileInfo remote);

	boolean shouldDeleteExtraFileFromRemote();

	//boolean shouldDeleteRemoteConflictFile(FileInfo local, FileInfo remote);
	
}
