package com.lianyuz.boxsync.core.policies;

import com.lianyuz.boxsync.core.file.FileInfo;

public class EqualPolicy implements BasePolicy {

	@Override
	public boolean shouldCopyMissingFileToLocal() {
		return true;
	}

	@Override
	public boolean shouldOverwriteLocalConflictFile(FileInfo local,
			FileInfo remote) {
		//local file is most recent
		if (local.isMoreRecentlyModifiedThan(remote))
			return false;
		else
			return true;
	}

	@Override
	public boolean shouldDeleteExtraFileFromLocal() {
		return false;
	}

	/*@Override
	public boolean shouldDeleteLocalConflictFile(FileInfo local,
			FileInfo remote) {
		if (local.isMoreRecentlyModifiedThan(remote))
			return false;
		else
			return true;
	}*/

	@Override
	public boolean shouldCopyMissingFileToRemote() {
		return true;
	}

	@Override
	public boolean shouldOverwriteRemoteConflictFile(FileInfo local,
			FileInfo remote) {
		if (remote.isMoreRecentlyModifiedThan(local))
			return false;
		else
			return true;
	}

	@Override
	public boolean shouldDeleteExtraFileFromRemote() {
		return false;
	}

	/*@Override
	public boolean shouldDeleteRemoteConflictFile(FileInfo local,
			FileInfo remote) {
		if (remote.isMoreRecentlyModifiedThan(local))
			return false;
		else
			return true;
	}*/
	
}
