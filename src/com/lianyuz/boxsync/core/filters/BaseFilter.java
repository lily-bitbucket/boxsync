package com.lianyuz.boxsync.core.filters;

public interface BaseFilter {
	public boolean isFiltered(String fileName);
}
