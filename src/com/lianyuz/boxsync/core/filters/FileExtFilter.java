package com.lianyuz.boxsync.core.filters;

import java.util.LinkedList;
import java.util.List;

public class FileExtFilter implements BaseFilter {

	List<String> extsToFilter = new LinkedList<String>();
	
	@Override
	public boolean isFiltered(String fileName) {
		if (fileName != null) {
			for ( String ext : extsToFilter )
				if ( fileName.endsWith(ext) )
					return true;
			return false;
		}
		else // fileName is null, if this happens, it should be filtered
			return true;
	}
	
	public FileExtFilter(List<String> extsToFilter) {
		if ( extsToFilter != null )
			this.extsToFilter.addAll(extsToFilter);
	}
	
}
