package com.lianyuz.boxsync.core.filters;

import java.util.LinkedList;
import java.util.List;


public class SyncFilterMng {
	
	public List<BaseFilter> filters = new LinkedList<BaseFilter>();
	
	public void addFilter(BaseFilter filter) {
		filters.add(filter);
	}
	
	public boolean isFiltered(String fileName) {
		for ( BaseFilter bf : filters ) {
			if ( bf.isFiltered(fileName) )
				return true;
		}
		return false;
	}
}
